# README #

This README documents whatever steps are necessary to get the application 'app'
up and running.

### What is this repository for? ###

Provides a minimal application developer infrastructure for deal.II frontend
applications.

### How do I get set up? ###

* Summary of set up


```
#!bash

cd app
cmake .
make debug
make info
make run
```


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Pricipial Author: Dipl.-Ing. Uwe Koecher (koecher@hsu-hamburg.de)

### License ###
Copyright (C) 2012-2015 by Uwe Koecher

'app' is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

'app' is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with 'app'. If not, see <http://www.gnu.org/licenses/>.
Please see the file
	./LICENSE
for details.