/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2016-09-23, UK
 * @date 2015-02-26, UK
 *
 * @brief DTM++/app.file: input / output file processing
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <vector>


int main(int argc, char *argv[]) {
	try {
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		// Check if an input file is given.
		AssertThrow(
			!(argc < 2),
			dealii::ExcMessage (
				std::string ("===>\tUSAGE: ./biot <Input_File>"))
		);
		
		// Check if the given input  file can be opened.
		const std::string input_filename(argv[1]);
		std::ifstream input_file(input_filename.c_str());
		// Test if an Input File is potentially given.
		AssertThrow(
			input_file,
			dealii::ExcMessage (
				std::string ("===>\tERROR: Input file <")
				+ input_filename + "> not found."
			)
		);
		
		std::string line;
		
		// skip first line
		std::getline(input_file, line);
		
		// process data lines
		std::vector< std::pair< double, unsigned int > > result_fine;
		std::vector< std::pair< double, unsigned int > > result_notfine;
		unsigned int i={9};
		while ( std::getline(input_file, line) ) {
			// read line
			std::stringstream process_line(line);
			double omega;
			unsigned int number;
			
			process_line >> omega;
			process_line >> number;
			
			result_fine.push_back(
				std::make_pair(1./omega, number)
			);
			
			if (i++ == 9) {
				result_notfine.push_back(
					std::make_pair(1./omega, number)
				);
				
				i = 0;
			}
		}
		input_file.close();
		
		{
			std::ofstream output_file(
				static_cast< std::string >(input_filename + "_fine_tikz.txt").c_str()
			);
			
			// write output
			auto rit = result_fine.rbegin();
			auto end = result_fine.rend();
			for ( ; rit != end; ++rit) {
				output_file << rit->first << " " << rit->second << "\\" << "\\" << std::endl;
			}
			
			output_file.close();
		}
		
		{
			std::ofstream output_file(
				static_cast< std::string >(input_filename + "_notfine_tikz.txt").c_str()
			);
			
			// write output
			auto rit = result_notfine.rbegin();
			auto end = result_notfine.rend();
			for ( ; rit != end; ++rit) {
				output_file << rit->first << " " << rit->second << "\\" << "\\" << std::endl;
			}
			
			output_file.close();
		}
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////
	}
	catch (std::exception &exc) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
					<< std::endl;
		
		std::cerr	<< exc.what() << std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		return 1;
	}
	catch (...) {
		std::cerr	<< std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl
					<< "An UNKNOWN EXCEPTION occured!"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl << std::endl
					<< "Further information:" << std::endl
					<< "\tThe main() function catched an exception"
					<< std::endl
					<< "\twhich is not inherited from std::exception."
					<< std::endl
					<< "\tYou have probably called 'throw' somewhere,"
					<< std::endl
					<< "\tif you do not have done this, please contact the authors!"
					<< std::endl << std::endl
					<< "----------------------------------------"
					<< "----------------------------------------"
					<< std::endl;
		
		std::cerr	<< std::endl
					<< "APPLICATION TERMINATED unexpectedly due to an exception."
					<< std::endl << std::endl
					<< "****************************************"
					<< "****************************************"
					<< std::endl << std::endl;
		
		return 1;
	}
	
	return 0;
}
